#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <strings.h>


#define NUMLETRAS ('z' -'a' + 1)

int main(int argc, char *argv[]){
    const char * const texto = "Me llamo Alejandro";
    const char * const palabras = "Amo";
    unsigned freq[NUMLETRAS];
    bzero(freq, sizeof(freq));

    for (const char *dedo = texto; *dedo != '\0'; dedo++)
        freq[*dedo - 'a']++;

    for (const char *letra = palabras; *letra = texto; letra++)
        printf ("%c: %.2lf%\n",*letra );

    return EXIT_SUCCESS;
}

