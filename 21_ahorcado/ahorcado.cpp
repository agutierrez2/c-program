#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define FILENAME "spanish_words.txt"
#define N 0x100
#define NUM 48247

void dibuja() {

    printf("\n");

    system("clear");
    system("toilet -fpagga -Fcrop:border:metal '   AHORCADO   '");

    printf("\n\n\n\t");
}

void get_word (char *word) {

    FILE *pf;
    char buffer[N];
    int len, i;
    char **palabra = NULL;

    if ( !(pf = fopen(FILENAME, "r")) ) {
        fprintf (stderr, "No... no veo nada.\n");
        exit (EXIT_FAILURE);
    }

    palabra = (char **) malloc (sizeof(char *));
    palabra = (char **) calloc(NUM, sizeof(char*));

    for(i=0; i<NUM; i++) { //48250
         do{
         palabra = (char **) realloc (palabra, (i+1) * sizeof(char *));
        fgets(buffer, N-1, pf);

        len = strlen (buffer);
        palabra[i] = (char *) realloc(palabra[i], len);
        strncpy (palabra[i], buffer, len);
        palabra[i][len-1] = '\0'; // Si pones solo len se queda el \n
    }

    free(palabra[i-1]);
    palabra[i-1] = NULL;

    strcpy(word, palabra[rand() % NUM]);

    // Cerrado de Datos dinámicos
    for(int j=0; palabra[j]; j++)
        free (palabra[j]);

    fclose(pf);
    free (palabra);
    }

    char get_letter () {

        char letter;
        bool comp = false;

        printf("Dime una letra: ");
        scanf(" %c", &letter);

        // Comprueba que esa letra no esté puesta y sea una letra minúscula
         for(int i=0; i<N; i++)
         if( letter == letra_puesta[i] )
         comp = true;

         if(!comp) {
        if(letter > 96 && letter < 123)
            return letter;
        else{
            printf("\nSolo puedes introducir letras minúsculas, %c\n", letter);
            get_letter();
        }
         }
         else {
         printf("Esa letra ya la has puesto\n");
         get_letter();
         }

        return letter;
    }

    bool comprobacion (char *word1, char *word2, char letter) {

        for(int i=0; i<strlen(word1)+1; i++)
            if(word1[i] == letter)
                word2[i] = letter;
    }

    int main (int argc, char *argv[]) {

        srand(time(NULL));

        char wordA[N] = "hola";
        char wordA[N];
        char wordR[N];
        char *letra_puesta;
        bool end = false;

        get_word(wordA);

        strcpy(wordR, wordA);

        for(int i=0; i<strlen(wordR); i++)
            wordR[i] = '_';

        do {

            dibuja();

            for(int i=0; i<strlen(wordR); i++)
                printf("%c", wordR[i]);

            printf("\n\n\nLetras ya puestas: ");
             for(int i=0; i<; i++)
             printf("%c", letra_puesta[i]);

            printf("\n\n");

            comprobacion (wordA, wordR, get_letter());

        }while(strcmp(wordR, wordA) != 0);

        dibuja();
        printf("%s  ", wordR);

        if (strcmp(wordR, wordA) == 0)
            printf("\n\n    HAS ACERTADO LA PALABRA\n\n");
        else
            printf("\n\n    HAS FALLADO HAS MUERTO\n\n");

        return EXIT_SUCCESS;
    }

