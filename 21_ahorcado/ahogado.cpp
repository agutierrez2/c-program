#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME "spanish_words.txt"
#define MAX 0x100
#define LMUERTE 10

#define RED_ON   "\x1B[31m"
#define BLUE_ON  "\x1B[34m"
#define WHITE_ON "\x1B[97m"
#define CYAN_ON  "\x1B[36m"
#define RESET    "\x1B[00m"

bool es_mala (char letra) {
    return true;
}

void principio (char *l, char *m) {

    system ("clear");
    system ("toilet -fpagga -F metal:border 'AHORCADO' ");

    __fpurge (stdin);

    printf ("\nLetras bien: " BLUE_ON "%s" RESET "\n", l); // \x1B[31m = cambia de color la terminal
    printf ("Letra mal: " RED_ON "%s" RESET "\n", m);
    printf (WHITE_ON "==========================\n\n" RESET);
}

void rellena_palabra (char *p) {

    FILE *pf;
    char buffer[MAX];
    char **palabra = NULL;
    int len, i;

    if ( !(pf = fopen(FILENAME, "r")) ) {
        fprintf (stderr, "No... no veo nada parece ser que era ciego al final\n"
                "y no eran las copas de demas.\n");
        exit (EXIT_FAILURE);
    }

    for(i = 0; i < 100; i++) {

        palabra = (char **) realloc (palabra, (i+1) * sizeof(char *));

        fgets (buffer, MAX-1 , pf);
        len = strlen (buffer);

        palabra[i] = (char *) malloc(len);
        palabra[i][len-1] = '\0';
    }

    free(palabra[i-1]);
    palabra[i-1] = NULL;

    int num_rand = rand() % i;
    strcpy ( p, palabra[0] );
    /*
     *     palabra = (char **) realloc (palabra, 1 * sizeof(char *));
     *
     *         fgets (buffer, MAX-1 , pf);
     *             len = strlen (buffer);
     *
     *                 palabra[0] = (char *) malloc(len);
     *                     palabra[0][len-1] = '\0';
     *
     *                         palabra = (char **) realloc (palabra, 2 * sizeof(char *));
     *                             fgets (buffer, MAX-1 , pf);
     *                                 len = strlen (buffer);
     *
     *                                     palabra[1] = (char *) malloc(len);
     *                                         palabra[1][len-1] = '\0';
     *                                             */
    for(int j=0; palabra[j]; j++)
        free (palabra[j]);

    free (palabra[0]);
    free (palabra[1]);
    free (palabra);

    fclose (pf);
}

int main (int argc, char *argv[]) {

    char *malas, *letras, letra, *palabra;
    int total_malas = 0, total_letras = 0;

    malas = (char *) malloc (LMUERTE + 1);
    letras = (char *) malloc (MAX + 1);
    bzero (malas, LMUERTE + 1);
    bzero (letras, MAX + 1);

    rellena_palabra (palabra);

    FILE *pf;
    char buffer[MAX];
    char **palabra = NULL;
    int len, i;

    if ( !(pf = fopen(FILENAME, "r")) ) {
        fprintf (stderr, "No... no veo nada parece ser que era ciego al final\n"
                "y no eran las copas de demas.\n");
        exit (EXIT_FAILURE);
    }

    for(i = 0; i < 100; i++) {

        palabra = (char **) realloc (palabra, (i+1) * sizeof(char *));

        fgets (buffer, MAX-1 , pf);
        len = strlen (buffer);

        palabra[i] = (char *) malloc(len);
        palabra[i][len-1] = '\0';
    }

    free(palabra[i-1]);
    palabra[i-1] = NULL;

    int num_rand = rand() % i;
    strcpy ( p, palabra[0] );

    for(int j=0; palabra[j]; j++)
        free (palabra[j]);

    free (palabra[0]);
    free (palabra[1]);
    free (palabra);

    fclose (pf);
    while (total_malas < LMUERTE) {

        principio (letras, malas);

        printf ( CYAN_ON "Dime letra: " );
        letra = getchar();
        printf ( RESET "\n" );

        if( !(letra >= 97 && letra <= 122) )
            continue;

        if( strchr (letras, letra) )
            continue;

        if (es_mala (letra)) // stub = una funcion que simula que hay una funcion pero devuelve siempre true
            *(malas + total_malas++) = letra;

        *(letras + total_letras++) = letra;

        printf("\n");
    }

    free (letras);
    free (malas);

    return EXIT_SUCCESS;
}

