#include <stdio.h>
#include <stdlib.h>

/*Función punto de entrada*/
int main(){
    int resultado; 
    int a;
    int b;    
    int opcion;

    printf("Tenemos las siguientes opciones 1:Cuadrado, 2:Rectángulo y 3:Rombo\n");
    printf("Indique la opción que quieres utilizar\n: ");
    scanf("%i", &opcion);
    printf("Usted ha elegido %i\n", opcion);

    switch(opcion)
    {
        case 1:
            printf("\n Valor del lado: ");
            scanf("%i", &a);
            resultado=a*a;
            printf("\nEl área del cuadrado es: %i\n", resultado);
            break;

        case 2:
            printf("\n Valor de la altura: ");
            scanf("%i", &a);
            printf("\n Valor de la base: ");
            scanf("%i", &b);
            resultado=a*b;
            printf("\nEl area del rectángulo es: %i\n", resultado);
            break;

        case 3:
            printf("\n Valor de la diagonal mayor: ");
            scanf("%i", &a);
            printf("\n Valor de la diagonal menor: ");
            scanf("%i", &b);
            resultado=(a*b)/2;
            printf("\nEl área del rombo es: %i\n", resultado);
            break;

    }




    return EXIT_SUCCESS;

}
