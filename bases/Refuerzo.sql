DROP DATABASE IF EXISTS Ejercicio1;
CREATE DATABASE  Ejercicio1;
USE  Ejercicio1;

CREATE TABLE CLIENTE(DNI VARCHAR(9), nombre VARCHAR(20), apellidos VARCHAR(40), telefono TINYINT(9), email VARCHAR(30), PRIMARY KEY (DNI));

CREATE TABLE TIENDA(nombre VARCHAR(20), provincia VARCHAR(20), localidad VARCHAR(20), direccion VARCHAR(20),
telefono TINYINT(9), dia_apertura DATE, dia_cierre DATE, hora_apertura TIME, hora_cierre TIME, PRIMARY KEY (nombre));

CREATE TABLE OPERADORA(nombre VARCHAR(20), color_logo VARCHAR(15), porcentaje_cobertura CHAR(3),
frecuenciaGSM VARCHAR(10), pagina_web VARCHAR(20), PRIMARY KEY(nombre));

CREATE TABLE TARIFA(nombre VARCHAR(20), nombre_operadora VARCHAR(20), tamaño_datos VARCHAR(20), 
tipo_datos VARCHAR(10), minutos_gratis VARCHAR(10), SMSgratis VARCHAR(50), PRIMARY KEY(nombre), FOREIGN KEY (nombre_operadora) REFERENCES OPERADORA(nombre));

CREATE TABLE MOVIL(marca VARCHAR(20), modelo VARCHAR(20), descripcion VARCHAR(20), SO VARCHAR(20),
 RAM VARCHAR(10), pulgadas_pantalla VARCHAR(10), camara_mpx VARCHAR(10), PRIMARY KEY(marca,modelo));

CREATE TABLE MOVIL_LIBRE(marca_movil VARCHAR(20), modelo_movil VARCHAR(20), precio VARCHAR(20),
FOREIGN KEY (marca_movil, modelo_movil) REFERENCES MOVIL(marca, modelo));

CREATE TABLE MOVIL_CONTRATO(marca_movil VARCHAR(20), modelo_movil VARCHAR(20), nombre_operadora VARCHAR(20), precio VARCHAR(20),
FOREIGN KEY(marca_movil, modelo_movil) REFERENCES MOVIL(marca,modelo), FOREIGN KEY(nombre_operadora) REFERENCES OPERADORA(nombre));

CREATE TABLE OFERTA(nombre_operadora_tarifa VARCHAR(20), nombre_tarifa VARCHAR(20), marca_movil_contrato VARCHAR(20), modelo_movil_contrato VARCHAR (20), dia DATE,
FOREIGN KEY (nombre_operadora_tarifa, nombre_tarifa) REFERENCES TARIFA(nombre_operadora, nombre), FOREIGN KEY(marca_movil_contrato, modelo_movil_contrato) REFERENCES MOVIL_CONTRATO(marca_movil, modelo_movil));

CREATE TABLE COMPRA(DNI_cliente VARCHAR(20), nombre_tienda VARCHAR(20), marca_movil_libre VARCHAR(20), modelo_movil_libre VARCHAR(20), dia DATE, 
FOREIGN KEY(DNI_cliente) REFERENCES CLIENTE(DNI), FOREIGN KEY(nombre_tienda) REFERENCES TIENDA(nombre), FOREIGN KEY(marca_movil_libre, modelo_movil_libre) REFERENCES MOVIL_LIBRE(marca_movil, modelo_movil));

CREATE TABLE CONTRATO(DNI_cliente VARCHAR(20), nombre_tienda VARCHAR(20), nombre_operadora_tarifa_oferta VARCHAR(20), nombre_tarifa_oferta VARCHAR(20), 
marca_movil_oferta VARCHAR(20), modelo_movil_oferta VARCHAR(20), dia DATE, 
FOREIGN KEY(DNI_CLIENTE) REFERENCES CLIENTE(DNI), FOREIGN KEY(nombre_tienda) REFERENCES TIENDA(nombre), 
FOREIGN KEY(nombre_operadora_tarifa_oferta, nombre_tarifa_oferta, marca_movil_oferta, modelo_movil_oferta) REFERENCES OFERTA(nombre_operadora_tarifa, nombre_tarifa, marca_movil_contrato, modelo_movil_contrato));

